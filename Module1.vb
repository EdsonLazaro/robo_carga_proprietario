﻿Imports System.IO
Imports System.Net
Imports System.Text
Imports System.Math
Imports System.Data
Imports System.Text.RegularExpressions
Imports MySql.Data.MySqlClient
Module Module1

    Dim caminho1 As String = "C:\LOGXMLS\BASE\BN_BE\" '"C:\LOGXMLS\"
    Dim local As String = "C:\salvarTextoProp\" ' & Date.Now.ToString("ddMMyyyy") & "\"
    Sub Main()
        Dim i As String = 0

        '   For Each arquivos As String In Directory.GetDirectories(caminho1)

        For Each local2 As String In Directory.GetDirectories(caminho1)


            '  For Each endereco As String In Directory.GetFiles(local2)


            Dim lista As New List(Of String)
            For Each arq As String In Directory.GetFiles(local2)
                Dim arquivo As New FileInfo(arq)
                If Regex.IsMatch(arquivo.Name.ToLower, "\.(csv|pdf|txt|dig|htm|html|xml)$") Then
                    Dim texto As String = ""
                    Dim reader As New StreamReader(arquivo.FullName, System.Text.ASCIIEncoding.Default)
                    '  texto = limpaHtml(reader.ReadToEnd)
                    texto = reader.ReadToEnd
                    reader.Close()

                    If Not String.IsNullOrEmpty(texto) Then
                        Dim fornecedor As String = Regex.Match(texto, "<FORNECEDOR>[^<]+", RegexOptions.IgnoreCase).Value

                        If Regex.IsMatch(fornecedor, "MOTOCONSULTA", RegexOptions.IgnoreCase) Then
                            Recorta_Motoconsulta(texto)
                        ElseIf Regex.IsMatch(fornecedor, "RENE", RegexOptions.IgnoreCase) Then
                            Recorta_Rene(texto)
                        ElseIf Regex.IsMatch(fornecedor, "RG", RegexOptions.IgnoreCase) Then
                            Recorta_RG(texto)
                        ElseIf Regex.IsMatch(fornecedor, "CHECKPRO", RegexOptions.IgnoreCase) Then
                            Recorta_Checkpro(texto)
                        ElseIf Regex.IsMatch(fornecedor, "AUTOCREDCAR", RegexOptions.IgnoreCase) Then
                            Recorta_AutoCredCAr(texto)

                        End If
                    End If
                End If
                'Next
            Next
            Threading.Thread.Sleep(300)
        Next
        '    Next


    End Sub

    Public Sub Recorta_Motoconsulta(ByVal TEXTO As String)
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then  'CPFPROPRIETARIO   CPFCNPJPROPRIETARIO 5510337893

            Dim nome As String = Regex.Match(TEXTO, "NOMEPROPRIETARIO>[^<]+", RegexOptions.IgnoreCase).Value.Replace("NOMEPROPRIETARIO", "").Replace(">", "")
            Dim documento As String = Regex.Match(TEXTO, "CPFPROPRIETARIO>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CPFPROPRIETARIO", "").Replace(">", "").Replace(".", "").Replace("-", "").Replace("/", "")
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PLACA", "").Replace(">", "")
            Dim chassi As String = Regex.Match(TEXTO, "CHASSI>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CHASSI", "").Replace(">", "")
            Dim renavam As String = Regex.Match(TEXTO, "renavam>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RENAVAM", "").Replace(">", "")
            Dim municipio As String = Regex.Match(TEXTO, "MUNICIPIODESC>[^<]+", RegexOptions.IgnoreCase).Value.Replace("MUNICIPIODESC", "").Replace(">", "")
            Dim uf As String = Regex.Match(TEXTO, "uf>[^<]+", RegexOptions.IgnoreCase).Value.Replace("UF", "").Replace(">", "")
            Dim propAnterior As String = Regex.Match(TEXTO, "NOMEPROPRIETARIOANTERIOR>[^<]+", RegexOptions.IgnoreCase).Value.Replace("NOMEPROPRIETARIOANTERIOR", "").Replace(">", "")
            Dim restricao As String = Regex.Match(TEXTO, "restricao>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RESTRICAO", "").Replace(">", "")
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "dtAtualizacao>[^<]+", RegexOptions.IgnoreCase).Value.Replace("DTATUALIZACAO", "").Replace(">", "")

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim

            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                '  If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
                '   End If
            End If

        End If



    End Sub

    Public Sub Recorta_Rene(ByVal TEXTO As String)
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then

            Dim nome As String = Regex.Match(TEXTO, "PRONOME>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PRONOME", "").Replace(">", "")
            Dim documento As String = Regex.Match(TEXTO, "CPF_CNPJ_PROPRIETARIO>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CPF_CNPJ_PROPRIETARIO", "").Replace(">", "")
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PLACA", "").Replace(">", "")
            Dim chassi As String = Regex.Match(TEXTO, "chassi>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CHASSI", "").Replace(">", "")
            Dim renavam As String = Regex.Match(TEXTO, "renavam>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RENAVAM", "").Replace(">", "")
            Dim municipio As String = Regex.Match(TEXTO, "municipio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("UF", "").Replace(">", "")
            Dim uf As String = Regex.Match(TEXTO, "uf>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CPF_CNPJ_PROPRIETARIO", "").Replace(">", "")
            Dim propAnterior As String = Regex.Match(TEXTO, "propAnterio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PROPANTERIO", "").Replace(">", "")
            Dim restricao As String = Regex.Match(TEXTO, "RESTRICAOFINAN>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RESTRICAOFINAN", "").Replace(">", "")
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "dtAtualizacao>[^<]+", RegexOptions.IgnoreCase).Value.Replace("DTATUALIZACAO", "").Replace(">", "")

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim

            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                '  If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
                ' End If
            End If

        End If



    End Sub

    Public Sub Recorta_RG(ByVal TEXTO As String)
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then

            Dim nome As String = Regex.Match(TEXTO, "Proprietario>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PROPRIETARIO", "").Replace(">", "")
            Dim documento As String = Regex.Match(TEXTO, "CpfCnpjProprietario>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CPFCNPJPROPRIETARIO", "").Replace(">", "")
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PLACA", "").Replace(">", "")
            Dim chassi As String = Regex.Match(TEXTO, "chassi>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CHASSI", "").Replace(">", "")
            Dim renavam As String = Regex.Match(TEXTO, "renavam>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RENAVAM", "").Replace(">", "")
            Dim municipio As String = Regex.Match(TEXTO, "municipio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("MUNICIPIO", "").Replace(">", "")
            Dim uf As String = Regex.Match(TEXTO, "uf>[^<]+", RegexOptions.IgnoreCase).Value.Replace("UF", "").Replace(">", "")
            Dim propAnterior As String = Regex.Match(TEXTO, "ProprietarioAnterior>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PROPRIETARIOANTERIOR", "").Replace(">", "")
            Dim restricao As String = Regex.Match(TEXTO, "Restricao1>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RESTRICAO1", "").Replace(">", "")
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "DataUltimaAtualizacao>[^<]+", RegexOptions.IgnoreCase).Value.Replace("DATAULTIMAATUALIZACAO", "").Replace(">", "")

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim

            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                '  If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
            End If
            ' End If

        End If



    End Sub

    Public Sub Recorta_Denatran(ByVal TEXTO As String) ' NAO TEM PROPRIETERIO
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then

            Dim nome As String = Regex.Match(TEXTO, "fornecedor>[^<]+", RegexOptions.IgnoreCase).Value
            Dim documento As String = Regex.Match(TEXTO, "domcumento>[^<]+", RegexOptions.IgnoreCase).Value
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value
            Dim chassi As String = Regex.Match(TEXTO, "fornecedor>[^<]+", RegexOptions.IgnoreCase).Value
            Dim renavam As String = Regex.Match(TEXTO, "renavam>[^<]+", RegexOptions.IgnoreCase).Value
            Dim municipio As String = Regex.Match(TEXTO, "municipio>[^<]+", RegexOptions.IgnoreCase).Value
            Dim uf As String = Regex.Match(TEXTO, "uf>[^<]+", RegexOptions.IgnoreCase).Value
            Dim propAnterior As String = Regex.Match(TEXTO, "propAnterio>[^<]+", RegexOptions.IgnoreCase).Value
            Dim restricao As String = Regex.Match(TEXTO, "restricao>[^<]+", RegexOptions.IgnoreCase).Value
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "dtAtualizacao>[^<]+", RegexOptions.IgnoreCase).Value

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim

            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                '   If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
                ' End If
            End If

        End If



    End Sub

    Public Sub Recorta_AutoCredCAr(ByVal TEXTO As String)
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then

            Dim nome As String = Regex.Match(TEXTO, "nomeFinanciado>[^<]+", RegexOptions.IgnoreCase).Value.Replace("nomeFinanciado", "").Replace(">", "")
            Dim documento As String = Regex.Match(TEXTO, "docFinanciado>[^<]+", RegexOptions.IgnoreCase).Value.Replace("docFinanciado", "").Replace(">", "")
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PLACA", "").Replace(">", "")
            Dim chassi As String = Regex.Match(TEXTO, "chassi>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CHASSI", "").Replace(">", "")
            Dim renavam As String = Regex.Match(TEXTO, "renavan>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RENAVAN", "").Replace(">", "")
            Dim municipio As String = Regex.Match(TEXTO, "municipio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("MUNICIPIO", "").Replace(">", "")
            Dim uf As String = Regex.Match(TEXTO, "uf>[^<]+", RegexOptions.IgnoreCase).Value.Replace("UF", "").Replace(">", "")
            Dim propAnterior As String = Regex.Match(TEXTO, "propAnterio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PROPANTERIO", "").Replace(">", "")
            Dim restricao As String = Regex.Match(TEXTO, "msg>[^<]+", RegexOptions.IgnoreCase).Value.Replace("MSG", "").Replace(">", "")
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "dataHora>[^<]+", RegexOptions.IgnoreCase).Value.Replace("DATAHORA", "").Replace(">", "")

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim


            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                ' If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
                '  End If
            End If

        End If



    End Sub

    Public Sub Recorta_Checkpro(ByVal TEXTO As String)
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then

            Dim nome As String = Regex.Match(TEXTO, "NomeFinanciado>[^<]+", RegexOptions.IgnoreCase).Value.Replace("NOMEFINANCIADO", "").Replace(">", "")
            Dim documento As String = Regex.Match(TEXTO, "DocumentoFinanciado>[^<]+", RegexOptions.IgnoreCase).Value.Replace("DOCUMENTOFINANCIADO", "").Replace(">", "")
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PLACA", "").Replace(">", "")
            Dim chassi As String = Regex.Match(TEXTO, "chassi>[^<]+", RegexOptions.IgnoreCase).Value.Replace("CHASSI", "").Replace(">", "")
            Dim renavam As String = Regex.Match(TEXTO, "renavam>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RENAVAM", "").Replace(">", "")
            Dim municipio As String = Regex.Match(TEXTO, "municipio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("MUNICIPIO", "").Replace(">", "")
            Dim uf As String = Regex.Match(TEXTO, "UfPlaca>[^<]+", RegexOptions.IgnoreCase).Value.Replace("UFPLACA", "").Replace(">", "")
            Dim propAnterior As String = Regex.Match(TEXTO, "propAnterio>[^<]+", RegexOptions.IgnoreCase).Value.Replace("PROPANTERIO", "").Replace(">", "")
            Dim restricao As String = Regex.Match(TEXTO, "restricao>[^<]+", RegexOptions.IgnoreCase).Value.Replace("RESTRICAO", "").Replace(">", "")
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "dtAtualizacao>[^<]+", RegexOptions.IgnoreCase).Value.Replace("DTATUALIZACAO", "").Replace(">", "")

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim

            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                ' If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
                '  End If
            End If

        End If

    End Sub

    Public Sub Recorta_Consulcar(ByVal TEXTO As String) ' NAO TEM PROPRIETERIO
        TEXTO = TEXTO.ToUpper.Replace("'", "").Trim
        If Not String.IsNullOrEmpty(TEXTO) Then

            Dim nome As String = Regex.Match(TEXTO, "fornecedor>[^<]+", RegexOptions.IgnoreCase).Value
            Dim documento As String = Regex.Match(TEXTO, "domcumento>[^<]+", RegexOptions.IgnoreCase).Value
            Dim placa As String = Regex.Match(TEXTO, "placa>[^<]+", RegexOptions.IgnoreCase).Value
            Dim chassi As String = Regex.Match(TEXTO, "chassi>[^<]+", RegexOptions.IgnoreCase).Value
            Dim renavam As String = Regex.Match(TEXTO, "renavam>[^<]+", RegexOptions.IgnoreCase).Value
            Dim municipio As String = Regex.Match(TEXTO, "municipio>[^<]+", RegexOptions.IgnoreCase).Value
            Dim uf As String = Regex.Match(TEXTO, "uf>[^<]+", RegexOptions.IgnoreCase).Value
            Dim propAnterior As String = Regex.Match(TEXTO, "propAnterio>[^<]+", RegexOptions.IgnoreCase).Value
            Dim restricao As String = Regex.Match(TEXTO, "restricao>[^<]+", RegexOptions.IgnoreCase).Value
            Dim dtAtualizacao As String = Regex.Match(TEXTO, "dtAtualizacao>[^<]+", RegexOptions.IgnoreCase).Value

            nome = nome.Replace("'", "").Trim
            documento = documento.Replace("'", "").Trim

            If Not String.IsNullOrEmpty(nome) And Not String.IsNullOrEmpty(documento) Then
                ' If Not CONFEREBanco(nome, documento, placa, chassi) Then
                ' Dim insert As String = "insert into `basepropeietario`.`dados` value ('" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "') "
                Dim insert As String = "'" & nome & "' , '" & documento & "' , '" & placa & "' , '" & chassi & "' , '" & renavam & "' , '" & municipio & "' , '" & uf & "' , '" & propAnterior & "' , '" & restricao & "' , '" & dtAtualizacao & "' "
                salva(insert)
                'INSERTBanco(insert)
                ' End If
            End If

        End If

    End Sub



    Public Function limpaHtml(ByVal texto As String) As String
        Return Regex.Replace(Regex.Replace(Regex.Replace(texto, "<[^>]*>|&[^;]+;", " "), "[\r\n\t]", " "), "[\s]{2,}", " ").Trim
    End Function

    Function INSERTBanco(ByVal query As String) As Boolean
        '`basepropeietario`.`dados` 
        Dim conexao As New MySqlConnection("Server=Localhost;user id=root;password=m3p1g5a8s9;database=basepropeietario")

        If conexao.State = ConnectionState.Closed Then
            conexao.Open()
        End If

        Dim command As New MySqlCommand(query, conexao)
        Dim reader = command.ExecuteReader
        reader.Close()

        If conexao.State = ConnectionState.Open Then
            conexao.Close()
        End If
        Return False
    End Function

    Function CONFEREBanco(ByVal nome As String, ByVal documento As String, ByVal placa As String, ByVal chassi As String) As Boolean
        '`basepropeietario`.`dados` 
        Dim conexao As New MySqlConnection("Server=Localhost;user id=root;password=m3p1g5a8s9;database=basepropeietario")
        nome = nome.Replace("'", "").Trim
        documento = documento.Replace("'", "").Trim
        If conexao.State = ConnectionState.Closed Then
            conexao.Open()
        End If
        Dim query As String = "select * from `basepropeietario`.`dados` where nome = '" & nome & "'and documento= '" & documento & "' and placa='" & placa & "' and chassi= '" & chassi & "' "
        Dim command As New MySqlCommand(query, conexao)
        ' Dim reader = command.ExecuteReader
        '  reader.Close()
        Dim existe As Boolean = False
        Dim reader = command.ExecuteReader
        existe = reader.Read
        reader.Close()
        '  Return existe
        If conexao.State = ConnectionState.Open Then
            conexao.Close()
        End If
        Return existe
    End Function

    Public Sub salva(ByVal arquivo As String)
        If Not IO.Directory.Exists(local) Then
            IO.Directory.CreateDirectory(local)
        End If
        Dim cmDados As String = local & "arquivo" & ".txt"
        Dim stream As New IO.StreamWriter(cmDados, IO.FileMode.Append)
        stream.WriteLine(arquivo)
        stream.Flush()
        stream.Close()
    End Sub
End Module
